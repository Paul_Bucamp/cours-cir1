<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <title>chat</title>
        <style>
            body{
                text-align: center;
            }
            form{
                margin: auto;
                display: flex;
                flex-direction: column;
                justify-content: space-between;
                border: 2px solid blue;
                height: 500px;
                width: 500px;
            }
            #chat{
                border: 1px dotted blue;
                height: 75%;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <h1>CHAT</h1>
        <form action="./" method="POST">
            <div id="chat">
                <?php
                    showMessage($bdd);
                ?>
            </div>
            <input type="text" name="username" id="username" placeholder="username" required/>
            <textarea name="message" id="message" rows="10" cols="30"></textarea>
            <input type="submit" value="Envoyer"/>
        </form>
    </body>
</html>

