<?php
    function insertion($bdd){
            $statement = $bdd->prepare('INSERT INTO messages(username, text, post_date) VALUES(:username, :text, :post_date)');
            $statement->execute([":username"=>$_POST['username'], ":text"=>$_POST['message'], ":post_date"=>date('y-M-d H:i:s')]);
    }
    
    function showMessage($bdd){
        $reponse = $bdd->query('SELECT * FROM messages');
        while($data=$reponse->fetch()){
            $tab=explode(' ', $data['text']);
            $firstString= $tab[0];
            if($firstString=='/me'){
                echo $data['post_date'].': <b>'.$data['username'].'</b>: <i>'.substr($data['text'], 3).'</i><br/>';
            }
            else{
                echo $data['post_date'].': <b>'.$data['username'].'</b>: '.$data['text'].'</br>';
            }
        }
    }