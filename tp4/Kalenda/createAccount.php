<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalenda</title>
        <link rel="shortcut icon" href="agenda.png">
        <style>
            form{
                margin: auto;
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                width: 35%;
            }
        </style>
    </head>
    <body>
        <form action="account.php" method="POST">
            <fieldset>
            <legend>Create Account</legend>
            <input type="text" id="login" name="login" placeholder="Login" required><br/><br/>
            <input type="password" id="password" name="password" placeholder="Password" required>
            <input type="password" id="verifyPassword" name="verifyPassword" placeholder="Confirm your password" required>
            <?php
            if(isset($valid)){
                if($valid==0){
                    echo '<p style="color: red">This login is already used !</p>';
                }
                else{
                    echo '<p style="color: red">Wrong Password</p>';
                }
            }
            ?>   
            <p>Organizer <input type="radio" id="rank" name="rank" value="ORGANIZER" required><br/>
            Customer <input type="radio" id="rank" name="rank" value="CUSTOMER" required></p>
            <input type="submit" value="Create Account"><p> You already have an account? <button type="button" onclick="window.location='./connectionPattern.php';">Log In</button></p> 
            </fieldset>
        </form>
    </body>
</html>


