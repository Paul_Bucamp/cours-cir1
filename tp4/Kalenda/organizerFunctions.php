<?php
session_start();
include 'dbFunctions.php';
surprise();
if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['hourEvent']) && isset($_POST['minuteEvent'])){
        $db= linkDb();
        createEvent($db);       //if we have everything to create an event, we create it
    }

if(isset($_GET['month'])){      //if previous or next button was clicked
        if($_GET['month']=='previous'){  
            $_SESSION['monthCalendar']--;
            if($_SESSION['monthCalendar']<1){
                $_SESSION['monthCalendar']=12;
                $_SESSION['yearCalendar']--;
            }
        }
        elseif($_GET['month']=='next'){
            $_SESSION['monthCalendar']++;
            if($_SESSION['monthCalendar']>12){
                $_SESSION['monthCalendar']=1;
                $_SESSION['yearCalendar']++;
            }
        }
    }
    
    if(isset($_GET['refresh'])){
        $_SESSION['yearCalendar']=$_POST['newYear'];
    }
    echo '<script>window.location="index.php";</script>';
//I use this script to avoid changing the month when we refresh the page