<?php
session_start();
if($_SESSION['currentUser']['state']!='connected'){
    include 'dbFunctions.php';
    surprise(); //surprise
    include 'connectionPattern.php';
}
else{
    unset($_SESSION['dayEvent']);
    unset($_SESSION['nameEvent']);
    unset($_SESSION['day']);
    include 'dbFunctions.php';
    surprise();
    $months = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $days = array(1=>'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
    $dayStart=1;
    //we want to know which day will begin the current month
    while(date('D', strtotime($_SESSION['yearCalendar'].'-'.$_SESSION['monthCalendar'].'-1')) != $days[$dayStart]){
        $dayStart++;
    }
    //$dayStart is now the right day for the 1st of the month
    include 'events'.htmlspecialchars($_SESSION['currentUser']['rank']).'.php';
}
