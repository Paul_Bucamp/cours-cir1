<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalenda</title>
        <link rel="shortcut icon" href="agenda.png">
        <style>
            body{
                text-align: center;
            }
            
            fieldset{
                display: flex;
                flex-direction: column;
                justify-content: space-between;
                margin: auto;
                width: 35%;
                height: 400px;
            }
            
            textarea{
                height: 40%;
            }
            
            p>input{
                width: 10%;
            }
        </style>
        <script>
            setTimeout(function autoDisconnect(){
                window.location="connectionPattern.php?disconnected";
            }, 600000);
        </script>
    </head>
    <body>
        <?php
        session_start();
        //if the user is connected and is an organizer
        if($_SESSION['currentUser']['state']=='connected' && $_SESSION['currentUser']['rank']=='ORGANIZER'){
        echo '<form action="organizerFunctions.php?day='.$_GET['day'].'" method="POST">
                <fieldset> 
                    <legend>Create Event</legend>
                    <input type="text" id="name" name="name" placeholder="Name" required/>
                    <textarea id="description" name="description" placeholder="Description"></textarea>
                    <p>Start: <input type="number" min="0" max="23" value="0" id="hourEvent" name="hourEvent" required/>h<input type="number" min="0" max="59" value="0" id="minuteEvent" name="minuteEvent" required/></p>

                    <p>End: <input type="number" min="0" max="23" value="0" id="hourEnd" name="hourEnd" required/>h<input type="number" min="0" max="59" value="0" id="minuteEnd" name="minuteEnd" required/></p>

                    <p>Places available: <input type="number" min="0" value="0" name="nb_place" id="nb_place" required/></p>

                    <p><button type="button" onclick="window.location=\'eventsORGANIZER.php\';" style="width: 49%"><< Return</button>
                        <input type="submit" value="Create >>" style="width: 49%"/></p>
                </fieldset>
            </form>';
        }else{
            echo 'You are not currently connected <button type="button" onclick="window.location=\'connectionPattern.php\';">Log In</button>';
        }
    ?>
    </body>
</html>

