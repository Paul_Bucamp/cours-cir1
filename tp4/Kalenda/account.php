<?php

include 'dbFunctions.php';
surprise();
$db = linkDb();
session_start();
unset($_SESSION['dayEvent']);
unset($_SESSION['nameEvent']);
$_SESSION['monthCalendar']=(int)date('m');
$_SESSION['yearCalendar']=(int)date('Y');
if(isset($_GET['enter'])){  //If the customer wants to enter an event
    enterEvent($db);
}
elseif(isset($_GET['leave'])){  //If the customer wants to leave an event
    leaveEvent($db);
}
elseif(isset($_GET['delete'])){  //If the organizer wants to delete an event
    deleteEvent($db);
}
elseif(isset($_POST['rank'], $_POST['login'], $_POST['password'])){  //If a new account is created
    newUser($db);    
}
elseif(isset($_POST['login'], $_POST['password'])){  //When the user wants to connect
    connectUser($db);
}


