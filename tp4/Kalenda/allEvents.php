<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalenda</title>
        <link rel="shortcut icon" href="agenda.png">
        <style>
            body{
                text-align: center;
            }
            
            .event{
                color: red;
                display: flex;
                justify-content: space-around;
            }
            
            #tableEvents{
                border: 4px solid black;
                border-radius: 9px;
                width: 100%;
            }
            
            .days{
                border: 1px solid black;
                border-radius: 5px;
                width: 14.29%;
                height: 150px;
            }
            #month>td{
                border: 1px solid black;
                border-radius: 5px;
                text-align: center;
                height: 3%;
            }
            
            #desc{
                width: 50%;
                margin: auto;
                border: 1px dotted black;
            }
        </style>
        <script>
            setTimeout(function autoDisconnect(){
                window.location="connectionPattern.php?disconnected";
            }, 600000);
        </script>
    </head>
    <body>
        <?php
            //this page is like eventsCUSTOMER but only for one day
            session_start();
            echo '<button type="button" onclick="window.location=\'connectionPattern.php\';">Disconnect</button><br/>';  
            if(!isset($_SESSION['day'])){
                $_SESSION['day']=$_GET['day'];  //same trick, changing the GET value won't do anything
            }
            $months = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        ?>
        <form>
            <fieldset>
                <legend>Event: <?php echo $_SESSION['yearCalendar'].' '.$months[$_SESSION['monthCalendar']].' '.$_SESSION['dayEvent'];?></legend>
                <?php 
                if($_SESSION['currentUser']['state']=='connected'){
                    include 'dbFunctions.php';
                    surprise();
                    $db= linkDb();
                    $answer=$db->query('SELECT name, nb_place, id FROM events WHERE DATE_FORMAT(startdate, \'%Y-%m-%d\')="'.date('Y-m-d', strtotime($_SESSION['yearCalendar'].'-'.$_SESSION['monthCalendar'].'-'.$_SESSION['day'])).'"');
                    $data=$answer->fetchAll();
                    echo '<p>Events:</p>';
                    foreach($data as $event){
                        $doesParticipate=$db->query('SELECT id_participant FROM user_participates_events WHERE id_event="'.$event[2].'"');
                        $doesParticipate=$doesParticipate->fetch();
                        $dataPlaces=$db->query('SELECT COUNT(*) FROM user_participates_events WHERE id_event="'.$event[2].'"');
                        $dataPlaces=$dataPlaces->fetch();
                        $nbPlaces= $event[1]-$dataPlaces[0];
                        if(($nbPlaces>0 || ($nbPlaces<0 || $doesParticipate[0]==$_SESSION['currentUser']['id']))){
                            echo '<p><a href="infoEvent.php?name='.$event[0].'&day='.$_SESSION['day'].'" class="event">'.$event[0].'</a></p>';  
                        }
                    }
                echo '<button type="button" onclick="window.location=\'events'.$_SESSION['currentUser']['rank'].'.php\';"><< Return</button>'; 
                }
                else{
                    echo 'You are not currently connected <button type="button" onclick="window.location=\'connectionPattern.php\';">Log In</button>';
                }
                ?>
            </fieldset>
        </form>
    </body>
</html>