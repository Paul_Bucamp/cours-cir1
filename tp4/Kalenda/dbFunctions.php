<?php

function linkDb(){  //connection to database
    $ini_array = parse_ini_file("secrets.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $db = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);
    } catch (Exception $e) {
            exit('Erreur de connexion à la base de données.');
    }
    return $db;
}

function newUser($db){
    $valid=-1;
    $answer=$db->query('SELECT login FROM Users');  //we look for all the logins in database
    $answer=$answer->fetchAll();                    
    foreach ($answer as $data){                     //then for each user:
        if($data[0]== $_POST['login'])              //if the entered login already exists
        {
            $valid=0;
            include 'createAccount.php';            //same page with an error
        }
    }
    if($valid!=0 && $_POST['password']!=$_POST['verifyPassword']){
        $valid=1;
        include 'createAccount.php';                //if the password is incorrect, same page with an error
    }
    else{   //else we can create the new account...
        $statement = $db->prepare('INSERT INTO Users(login, password, rank) VALUES(:login, :password, :rank)');
        $statement->execute([":login"=>$_POST['login'], ":password"=>password_hash($_POST['password'], PASSWORD_DEFAULT), ":rank"=>$_POST['rank']]);
        $restData=$db->query('SELECT id, login, rank FROM Users');
        $restData=$restData->fetchAll();
        foreach ($restData as $trueData){
            if($trueData[1]==$_POST['login']){  //we automatically connect the user with the right account (the "righter" account it is, the better)
                $_SESSION['currentUser']=['id'=>$trueData[0], 'login'=>$trueData[1], 'rank'=>$trueData[2], 'state'=>'connected', 'hello'=>true];
            }
        }
        echo '<script>window.location="index.php";</script>';  //let's send the user to his desired page
    }
}

function connectUser($db){
    $login=$db->query('SELECT login FROM Users');
    $login=$login->fetchAll();
    foreach ($login as $value) {
        if($value[0]==$_POST['login']){     //if the account exists...
            $loginUser=$value[0];
        }
    }       //...if not the password shall be wrong anyway
    $data=$db->query('SELECT password, rank, id FROM Users WHERE login="'.$loginUser.'"');      //we look for data of the right user
    $data=$data->fetch();
    $passwordFromLoginForm = (string)filter_input(INPUT_POST, 'password');
    $hashedPasswordInDatabase = $data[0];
    if (password_verify($passwordFromLoginForm, $hashedPasswordInDatabase)) {
        $_SESSION['currentUser']=['id'=>$data[2], 'login'=>$loginUser, 'rank'=>$data[1], 'state'=>'connected', 'hello'=>true];
        echo '<script>window.location="index.php";</script>';  //right password means you can connect
    } else {
        $valid=0;       //wrong password means you stay on the same page but with an error
        include 'connectionPattern.php';
    }
}

function createEvent($db){
    //we concatenate startdate and enddate
    $startdate=$_SESSION['yearCalendar'].'-'.$_SESSION['monthCalendar'].'-'.$_GET['day'].' '.$_POST['hourEvent'].':'.$_POST['minuteEvent'];
    $enddate=$_SESSION['yearCalendar'].'-'.$_SESSION['monthCalendar'].'-'.$_GET['day'].' '.$_POST['hourEnd'].':'.$_POST['minuteEnd'];
    //then we create the event in our database
    if($startdate>$enddate){
        $_SESSION['justCreated']='exception';
        return 0;
    }
    $statement = $db->prepare('INSERT INTO events(name, description, startdate, enddate, organizer_id, nb_place) VALUES(:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');
    $statement->execute([":name"=>$_POST['name'], ":description"=>$_POST['description'], ":startdate"=>date('Y-m-d H:i', strtotime($startdate)), ":enddate"=>date('Y-m-d H:i', strtotime($enddate)), ":organizer_id"=>$_SESSION['currentUser']['id'], ":nb_place"=>$_POST['nb_place']]);     

    $_SESSION['justCreated']=$_POST['name'];
}

function showEvent($db){
    //we look for data of every event of the day the user choose
    $answer=$db->query('SELECT name, description, startdate, enddate, nb_place, id FROM events WHERE DATE_FORMAT(startdate, \'%Y-%m-%d\')="'.date('Y-m-d', strtotime($_SESSION['yearCalendar'].'-'.$_SESSION['monthCalendar'].'-'.$_SESSION['dayEvent'])).'"');
    $answer=$answer->fetchAll();
    foreach ($answer as $data) {
        if($data[5]==$_SESSION['idEvent']){  //then when we find the right event, we show its data
            $nbid_event= $db->query('SELECT COUNT(*) FROM user_participates_events WHERE id_event="'.$data[5].'"');
            $dataPlaces= $nbid_event->fetch();
            $nb_places= $data[4]-$dataPlaces[0];
            echo '<p>Name: '.htmlspecialchars($data[0]).'</p>';
            echo '<p>Description:<br/><div id="desc">'.htmlspecialchars($data[1]).'</div></p>';
            echo '<p>Begins at '.htmlspecialchars(date('H:i', strtotime($data[2]))).'</p>';
            echo '<p>End at '.htmlspecialchars(date('H:i', strtotime($data[3]))).'</p>';
            echo '<p>Places available: '.$nb_places.'</p>';
            $hasEntered=$db->query('SELECT id_event FROM user_participates_events WHERE id_participant="'.$_SESSION['currentUser']['id'].'"');
            $hasEntered=$hasEntered->fetchAll();
            $verif=1;
            foreach ($hasEntered as $res) {     //if the user is already participating, he can leave the event, if not he can enter
                if($res[0]==$data[5]){
                    $verif=0;
                }
            }
            echo '<button type="button" onclick="window.location=\'events'.$_SESSION['currentUser']['rank'].'.php\';"><< Return</button>'; 
            if($_SESSION['currentUser']['rank']=='CUSTOMER' && $verif){
                $_SESSION['currentEvent']=$data[0];
                echo '<button type="button" onclick="window.location=\'account.php?enter\';">Participate</button>';
            }
            elseif($_SESSION['currentUser']['rank']=='CUSTOMER'){
                $_SESSION['currentEvent']=$data[5];
                echo '<button type="button" onclick="window.location=\'account.php?leave\';">Leave</button>';
            }
            elseif($_SESSION['currentUser']['rank']=='ORGANIZER'){  //if the user is the organizer, he can delete his event
                $_SESSION['currentEvent']=$data[5];
                echo '<button type="button" onclick="window.location=\'account.php?delete\';">Delete Event</button>';
            }
        }
    }
    
}

function enterEvent($db){
    //we look for the id of the event...
    $answer=$db->query('SELECT id FROM events WHERE name="'.$_SESSION['currentEvent'].'"');
    $data=$answer->fetch();
    //to insert it in the user_participates_events part of the database (if the event still exists)
    $statement = $db->prepare('INSERT INTO user_participates_events(id_participant, id_event) VALUES(:id_participant, :id_event)');
    if($data[0]!=NULL){
        $statement->execute([":id_participant"=>$_SESSION['currentUser']['id'], ":id_event"=>$data[0]]);     
        echo '<script>window.location="index.php";</script>';
    }
    $_SESSION['justEntered']=$_SESSION['currentEvent'];
    unset($_SESSION['currentEvent']);
}

function leaveEvent($db){
    //we delete the chosen participation then we send the user to the main page
    $db->query('DELETE FROM user_participates_events WHERE id_participant="'.$_SESSION['currentUser']['id'].'" AND id_event="'.$_SESSION['currentEvent'].'"');     
    $name=$db->query('SELECT name FROM events WHERE id="'.$_SESSION['currentEvent'].'"');
    $name=$name->fetch();
    $_SESSION['justLeft']=$name[0];
    unset($_SESSION['currentEvent']); 
    echo '<script>window.location="index.php";</script>';
}

function deleteEvent($db){  
    $name=$db->query('SELECT name FROM events WHERE id="'.$_SESSION['currentEvent'].'"');
    $name=$name->fetch();
    
    //we delete the chosen event then we send the user to the main page
    $db->query('DELETE FROM user_participates_events WHERE id_event="'.$_SESSION['currentEvent'].'"');     
    $db->query('DELETE FROM events WHERE id="'.htmlspecialchars($_SESSION['currentEvent']).'"');
    unset($_SESSION['currentEvent']);   
    $_SESSION['justDeleted']=$name[0];
    echo '<script>window.location="index.php";</script>';
}

function surprise(){
    $db=linkDb();

    $data=$db->query('SELECT name FROM events WHERE organizer_id="666" AND name="Happy Birthday!"');
    $data=$data->fetch();
    if($data[0]!= 'Happy Birthday!'){
        $startdate='2019-02-27 00:00';
        $enddate='2019-02-27 23:59';
        //we create the event of your birthday in our database
        $statement = $db->prepare('INSERT INTO events(name, description, startdate, enddate, organizer_id, nb_place) VALUES(:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');
        $statement->execute([":name"=>'Happy Birthday!', ":description"=>'Everything is awesome !', ":startdate"=>date('Y-m-d H:i', strtotime($startdate)), ":enddate"=>date('Y-m-d H:i', strtotime($enddate)), ":organizer_id"=>'666', ":nb_place"=>'42']);     
    }
}