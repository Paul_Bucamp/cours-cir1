<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalenda</title>
        <link rel="shortcut icon" href="agenda.png">
        <style>
            body{
                text-align: center;
            }
            
            .event{
                color: red;
                display: flex;
                justify-content: space-around;
            }
            
            #tableEvents{
                border: 4px solid black;
                border-radius: 9px;
                width: 100%;
            }
            
            .days{
                border: 1px solid black;
                border-radius: 5px;
                width: 14.29%;
                height: 150px;
            }
            #month>td{
                border: 1px solid black;
                border-radius: 5px;
                text-align: center;
                height: 3%;
            }
            
            #desc{
                width: 50%;
                margin: auto;
                border: 1px dotted black;
            }
        </style>
        <script>
            setTimeout(function autoDisconnect(){
                window.location="connectionPattern.php?disconnected";
            }, 600000);
        </script>
    </head>
    <body>
        <?php
            session_start();
            unset($_SESSION['day']);
            echo '<button type="button" onclick="window.location=\'connectionPattern.php\';">Disconnect</button><br/>';  
            if(!isset($_SESSION['dayEvent'])){  //with these lines, even if the user changes the GET values, this will change nothing
                $_SESSION['dayEvent']=$_GET['day'];
                $_SESSION['idEvent']=$_GET['id'];
            }
            $months = array(1=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        ?>
        <form>
            <fieldset>
                <legend>Event: <?php echo $_SESSION['yearCalendar'].' '.$months[$_SESSION['monthCalendar']].' '.$_SESSION['dayEvent'];?></legend>
                <?php 
                if($_SESSION['currentUser']['state']=='connected'){
                    include 'dbFunctions.php';
                    surprise();
                    $db= linkDb();
                    showEvent($db); //we show the data of the event
                }
                else{
                    echo 'You are not currently connected <button type="button" onclick="window.location=\'connectionPattern.php\';">Log In</button>';
                }
                ?>
            </fieldset>
        </form>
    </body>
</html>