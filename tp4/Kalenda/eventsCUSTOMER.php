<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalenda</title>
        <link rel="shortcut icon" href="agenda.png">
        <style>
            body{
                text-align: center;
            }
            
            .event{
                color: blue;
            }
            
            #tableEvents{
                border: 4px solid black;
                border-radius: 9px;
                width: 100%;
            }
            
            .days{
                border: 1px solid black;
                border-radius: 5px;
                width: 14.29%;
                height: 150px;
            }
            #month>td{
                border: 1px solid black;
                border-radius: 5px;
                text-align: center;
                height: 3%;
            }
            
            .allEvents{
                display: flex;
                flex-direction: row;
                justify-content: space-around;
            }
            
            .dayBorder{
                border-bottom: 0.15em solid black;
                border-top: 0.15em solid black;
            }
            
            .notif{
                text-align: center;
                background-color: blue;
                margin-left: 15%;
                padding: 2%;
                position: absolute;
                top: -30%;
                animation-name: animation;
                animation-delay: 0.5s;
                animation-duration: 2s;
                animation-direction: alternate;
                animation-iteration-count: 2;
            }
            
            @keyframes animation{
                from{
                    top: -30%;
                }
                to{
                    top: -1%;
                }
            }
        </style>
        <script>
            setTimeout(function autoDisconnect(){
                window.location="connectionPattern.php?disconnected";
            }, 600000);
        </script>
    </head>
    <body>                      
        <?php 
        session_start();
        if($_SESSION['currentUser']['hello']==true){
            $_SESSION['currentUser']['hello']= false;
            echo '<div class="notif">Hello<br/><br/>'.htmlspecialchars($_SESSION['currentUser']['login']).'!</div>';
        }
        if(isset($_SESSION['justEntered'])){
            echo '<div class="notif">You<br/>entered<br/>'.htmlspecialchars($_SESSION['justEntered']).'!</div>';
            unset($_SESSION['justEntered']);
        }
        if(isset($_SESSION['justLeft'])){
            echo '<div class="notif">You<br/>left<br/>'.htmlspecialchars($_SESSION['justLeft']).'!</div>';
            unset($_SESSION['justLeft']);
        }
        //same as the organizer one except for a few things
        if($_SESSION['currentUser']['state']=='connected' && $_SESSION['currentUser']['rank']=='CUSTOMER'){
            echo '<button type="button" onclick="window.location=\'connectionPattern.php\';">Disconnect</button><br/><br/>';
            echo '<table id="tableEvents"><tr id="month"><td><a href="organizerFunctions.php?month=previous"><button type="button">Previous</button></a></td><td id="month" colspan="5"><form action="organizerFunctions.php?refresh" method="POST">'.$months[$_SESSION['monthCalendar']].' <input name="newYear" type="number" min="1" value="'.$_SESSION['yearCalendar'].'" style="width: 8%"><input type="submit" value="Go to selected year" style="width:18%"></form></td><td><a href="organizerFunctions.php?month=next"><button type="button">Next</button></a></td><tr/>';
            echo '<tr><td>Monday</td><td>Tuesday</td><td>Wednesday</td><td>Thursday</td><td>Friday</td><td>Saturday</td><td>Sunday</td></tr>';
            $day=0;
            for($i=1; $i<=5; $i++){
                echo '<tr>';
                if($i==1){
                    for($k=1; $k<$dayStart;$k++){
                        echo '<td></td>';
                    }
                }
                for($j=1; $j<=7; $j++){
                    if($i==1 && $day==0){
                        $j=$j+$dayStart-1;
                    }
                    $day++;
                    if(checkdate($_SESSION['monthCalendar'], $day, $_SESSION['yearCalendar'])){
                        echo '<td class="days"><table class="allEvents">';
                        echo '<tr><td class="dayBorder">'.$day.'</td></tr>';
                        $nbEvents=0;
                        $db= linkDb();
                        $answer=$db->query('SELECT name, nb_place, id FROM events WHERE DATE_FORMAT(startdate, \'%Y-%m-%d\')="'.date('Y-m-d', strtotime($_SESSION['yearCalendar'].'-'.$_SESSION['monthCalendar'].'-'.$day)).'"');
                        $data=$answer->fetchAll();
                        foreach($data as $event){
                            $doesParticipate=$db->query('SELECT id_participant FROM user_participates_events WHERE id_event="'.$event[2].'"');
                            $doesParticipate=$doesParticipate->fetch();
                            $dataPlaces=$db->query('SELECT COUNT(*) FROM user_participates_events WHERE id_event="'.$event[2].'"');
                            $dataPlaces=$dataPlaces->fetch();
                            $nbPlaces= $event[1]-$dataPlaces[0];
                            //if there are no places we don't show the event
                            //except if the customer is already participating
                            if($nbEvents<5 && ($nbPlaces>0 || ($nbPlaces<0 || $doesParticipate[0]==$_SESSION['currentUser']['id']))){
                                echo '<tr><td><a href="infoEvent.php?id='.htmlspecialchars($event[2]).'&day='.$day.'" class="event">'.htmlspecialchars($event[0]).'</a></td></tr>';  
                            }
                            if($nbPlaces>0 || $doesParticipate[0]==$_SESSION['currentUser']['id']){
                                $nbEvents++;                                
                            }
                        }
                        if($nbEvents>5){
                            echo '<tr><td><a href="allEvents.php?day='.$day.'">More...</a></td></tr>';
                        }
                        echo '</table></td>';
                    }
                }
                echo '</tr>';
            }
            echo '</table>';
        }
        else{
            echo 'You are not currently connected <button type="button" onclick="window.location=\'connectionPattern.php\';">Log In</button>';
        }
        ?>
    </body>
</html>