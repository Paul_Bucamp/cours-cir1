<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Kalenda</title>
        <link rel="shortcut icon" href="agenda.png">
        <style>
            form{
                margin: auto;
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                width: 50%;
            }
        </style>
    </head>
    <body>
        <form action="account.php" method="POST">
            <fieldset>
            <legend>Log In :</legend>
                <input type="text" id="login" name="login" placeholder="Login" required>
                <input type="password" id="password" name="password" placeholder="Password" required>
                <?php
                    session_start();
                    $_SESSION['currentUser']=NULL;  //the user can't connect by clicking the previous arrow of the navigator 
                    if(isset($valid)){              //if he was connected before
                        echo '<p style="color: red">Login and/or password are not correct</p>';
                    }
                    if(isset($_GET['disconnected'])){
                        echo '<p style="color: red">You were disconnected because you were idle for 10 minutes</p>';
                    }
                ?>
                <input type="submit" value="Log In"><p> or create a new account <button type="button" onclick="window.location='createAccount.php';">Create Account</button></p> 
            </fieldset>
        </form>
    </body>
</html>
