<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>L'Epicerie - Ajouter un Article</title>
    <meta name="author" content="P.BUCAMP"/>
    <style>
        body{
            background-color: grey;
            text-align: center;
        }
        .input{
            border-radius: 5px;
            height:25px;
        }
        fieldset{
            margin: auto;            
            width: 50%;
            background-color: cadetblue;
            border: 1px solid black;
            border-radius: 10px;
        }
    </style>
  </head>
  <body>
      <h1>Ajouter un article</h1>
	<form action="./" method="POST" enctype="multipart/form-data">
            <fieldset>
                <p><label for="photo">Ajouter une photo:</label><br/><br/>
                <?php
                define('TARGET_DIRECTORY', './images/');
                if(!empty($_FILES['photo'])) {
                move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
                }
                ?>
                <input style="padding-left: 9%" type="file" name="photo" id="photo"/></p><br/>
                <p><label for="nom">Nom du produit:</label><br/><br/>
                <input class="input"  type="text" name="nom" id="nom" value="<?= (@isset($_POST['nom'])) ? $_POST['nom'] : ""; ?>"/></p><br/>
                <p><label for="prix">Prix du produit:</label><br/><br/>
                <input class="input"  type="number" name="prix" min="0" step="0.01" id="prix" value="<?= (@isset($_POST['prix'])) ? $_POST['prix'] : "0"; ?>"/>€</p><br/>
                <p><label for="nbreProduits">Nombre de produits disponibles:</label><br/><br/>
                <input class="input"  type="number" min="0" name="nbreProduits" id="nbreProduits" value="<?= (@isset($_POST['nbreProduits'])) ? $_POST['nbreProduits'] : "0"; ?>"/></p><br/>
                <p><input class="input"  type="submit" value="Submit"/></p><br/>
                <p><a href="listeProduits.php"><button type='button'>Page de vente</button></a></p>
            </fieldset>
	</form>
  </body>
</html>

